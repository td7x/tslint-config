"use strict";

exports.__esModule = true;

exports.config = {

  extends: [
    // https://github.com/blakeembrey/tslint-config-standard
    // tslint-config-standard extends tslint-config-eslint which extends tslint:latest
    "tslint-config-standard",

    // https://github.com/jonaskello/tslint-immutable
    "tslint-immutable",

    // https://github.com/Glavin001/tslint-clean-code
    "tslint-clean-code",

    // https://github.com/alexjoverm/tslint-config-prettier
    "tslint-config-prettier",
  ],
  rules: {

    // Tom's pref
    semicolon: false,
    "ordered-imports": true,
    "object-literal-sort-keys": [true, "ignore-case"],

    // Recommended built-in rules
    "no-var-keyword": true,
    "no-parameter-reassignment": true,
    "typedef": [true, "call-signature"],

    // Immutability rules
    "readonly-keyword": true,
    "readonly-array": true,
    "no-let": true,
    "no-object-mutation": true,
    "no-delete": true,
    "no-method-signature": true,

    // Functional style rules
    "no-this": true,
    "no-class": true,
    "no-mixed-interface": true,
    "no-expression-statement": true,
    "no-if-statement": true

  }
}