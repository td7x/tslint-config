# @td7x/tslint-config
TSLint config to help products clean functional code; compatible with Prettier 
and based on StandardJS.

## Installation

``` bash
$ npm i -D @td7x/tslint-confg
```

## Usage

tslint.json
```json
{
  "extends": ["@td7x/tslint-config"]
}
```
